package com.patrojascontreras.models.services;

import java.util.*;

import com.patrojascontreras.models.entities.User;

public interface UsersService {
    public User userCreate(User user);
    public User userUpdate(User user);
    public void deleteById(String idUser);
    public List<User> usersListAll();
    public List<User> usersListAllById(String id);
    public User getUserById(String id);
    public Optional<User> getUserByIdWithOptional(String id);
    public User existByLogin(String username);
}
