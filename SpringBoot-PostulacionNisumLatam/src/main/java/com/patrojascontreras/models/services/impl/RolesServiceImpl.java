package com.patrojascontreras.models.services.impl;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.patrojascontreras.models.entities.Role;
import com.patrojascontreras.models.entities.Roles;
import com.patrojascontreras.models.repositories.RolesRepository;
import com.patrojascontreras.models.services.RolesService;

@Service
public class RolesServiceImpl implements RolesService {

    @Autowired
    private RolesRepository rolesRepository;

    public Optional<Role> findByRoleName(Roles role) {
        return rolesRepository.findByRoleName(role);
    }
}
