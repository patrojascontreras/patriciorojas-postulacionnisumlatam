package com.patrojascontreras.models.services;

import java.util.*;

import com.patrojascontreras.models.entities.Role;
import com.patrojascontreras.models.entities.Roles;

public interface RolesService {

    public Optional<Role> findByRoleName(Roles role);
}
