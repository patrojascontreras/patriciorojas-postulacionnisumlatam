package com.patrojascontreras.controller;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.patrojascontreras.config.security.jwts.JwtTokenUtil;
import com.patrojascontreras.models.dtos.response.Message;
import com.patrojascontreras.models.dtos.response.users.UserCreateResponse;
import com.patrojascontreras.models.entities.Phone;
import com.patrojascontreras.models.entities.Role;
import com.patrojascontreras.models.entities.Roles;
import com.patrojascontreras.models.entities.User;
import com.patrojascontreras.models.services.RolesService;
import com.patrojascontreras.models.services.UsersService;
import com.patrojascontreras.validations.Validation;

@RestController
@Tag(name = "UserCreateController", description = "Operaciones para procesar datos de un Usuario")
@RequestMapping(value = "/api/admin/users")
public class UserCreateController {

    @Autowired
    private UsersService usersService;

    @Autowired
    private RolesService rolesService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Operation(summary = "Crear Usuario")
    @RequestMapping(value = "/sign-up", method = RequestMethod.POST)
    public ResponseEntity<Object> userInsert(@RequestBody User user) {

        try {
            Validation validation = new Validation();

            if(user.getName() == "") {
                return new ResponseEntity<>(new Message("El campo Nombre no puede estar vacío"), HttpStatus.BAD_REQUEST);
            }

            if(user.getEmail() == "") {
                return new ResponseEntity<>(new Message("El campo Email no puede estar vacío"), HttpStatus.BAD_REQUEST);
            }

            if(user.getPassword() == "") {
                return new ResponseEntity<>(new Message("El campo Password no puede estar vacío"), HttpStatus.BAD_REQUEST);
            }

            if(!validation.validarEmail(user.getEmail())) {
                return new ResponseEntity<>(new Message("El campo Email está inválido"), HttpStatus.BAD_REQUEST);
            }

            if(!validation.validarPassword(user.getPassword())) {
                return new ResponseEntity<>(new Message("El campo Contraseña está inválido"), HttpStatus.BAD_REQUEST);
            }

            User existByEmailLogin = usersService.existByLogin(user.getEmail());

            if(existByEmailLogin == null) {
                //Procesar creación de datos
                Set<Role> roles = new HashSet<>();

                User usrCreate = new User();

                boolean isActive = true;

                LocalDateTime myDateObj = LocalDateTime.now(ZoneId.of("America/Santiago"));

                DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("MMM dd, yyyy HH:mm:ss a");
                String formattedFinalDateTime = myDateObj.format(myFormatObj);

                String password = passwordEncoder.encode(user.getPassword());

                usrCreate.setName(user.getName());
                usrCreate.setEmail(user.getEmail());
                usrCreate.setPassword(password);
                usrCreate.setCreatedAt(formattedFinalDateTime);
                usrCreate.setUpdatedAt(formattedFinalDateTime);
                usrCreate.setIsActive(isActive);

                String[] roleArr = null;

                if(roleArr == null) {
                    roles.add(rolesService.findByRoleName(Roles.ROLE_USER).get());
                }

                usrCreate.setRoles(roles);

                //List de Phones
                List<Phone> phones = new ArrayList<>();
                for (Phone phoneIn : user.getPhones()) {
                    // new Phone
                    Phone phone = new Phone(phoneIn.getNumber(), phoneIn.getCitycode(), phoneIn.getCountrycode());
                    // set user to Phone
                    phone.setUser(usrCreate);
                    // add phone to list
                    phones.add(phone);
                }
                //Fin List de Phones

                //Agregar listado de Phones en User
                usrCreate.setPhones(phones);
                //Fin Agregar listado de Phones en User

                usersService.userCreate(usrCreate);
                //Fin Procesar creación de datos

                //Generación de token
                Authentication authentication = authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword()));

                SecurityContextHolder.getContext().setAuthentication(authentication);
                String token = jwtTokenUtil.generateJwtToken(authentication);
                //Fin Generación de token

                UserCreateResponse response = new UserCreateResponse(usrCreate.getIdUser(),
                        usrCreate.getCreatedAt(),
                        usrCreate.getUpdatedAt(),
                        usrCreate.getUpdatedAt(),
                        token,
                        isActive);

                return new ResponseEntity<>(response, HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(new Message("El correo ya registrado"), HttpStatus.OK);
            }
        } catch(Exception e) {
            return new ResponseEntity<>(new Message("Error interno en el servidor"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
