-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-02-2024 a las 16:26:33
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_desafio_tecnico_postulacion_nisumlatam`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `phone`
--

CREATE TABLE `phone` (
  `number` int(11) NOT NULL,
  `city_code` int(11) DEFAULT NULL,
  `country_code` varchar(15) DEFAULT NULL,
  `phone_user` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `phone`
--

INSERT INTO `phone` (`number`, `city_code`, `country_code`, `phone_user`) VALUES
(91540803, 25, '02', 'c1273773-194d-4ed3-ad38-fc87ac72a574'),
(91542275, 20, '02', 'c1273773-194d-4ed3-ad38-fc87ac72a574'),
(96109454, 25, '02', '80579c9c-d479-46a6-8bbc-29acb2d7a8cc'),
(96110923, 21, '02', '80579c9c-d479-46a6-8bbc-29acb2d7a8cc'),
(96660965, 21, '02', '36c42aa8-3ecc-4b0d-9b27-a12a45e5a249'),
(96862312, 25, '02', '36c42aa8-3ecc-4b0d-9b27-a12a45e5a249'),
(97150869, 23, '02', 'c1980397-4b1d-439a-b81b-e6f36bb0bd3a'),
(97505984, 17, '02', 'e383a440-6ad2-48bd-97cc-728986316c40'),
(97521239, 15, '02', '1935c9f3-30d4-4237-93e1-782c0fc0264e'),
(97717822, 22, '02', 'c1980397-4b1d-439a-b81b-e6f36bb0bd3a'),
(97719052, 22, '02', 'ece1ab31-dda6-4927-b113-1294b57bc4e5'),
(97722594, 23, '02', 'ece1ab31-dda6-4927-b113-1294b57bc4e5'),
(97780819, 25, '02', '9fbd119a-4f2f-400c-9e92-3513cb6b50ba'),
(97888123, 13, '02', '1935c9f3-30d4-4237-93e1-782c0fc0264e'),
(97891212, 25, '02', 'fa658202-a030-4d58-b50b-38c37ca19e47'),
(98009155, 15, '02', '6711d5bd-3629-4aea-95a7-6ea71fccfce7'),
(98017272, 18, '02', '6711d5bd-3629-4aea-95a7-6ea71fccfce7'),
(98091785, 17, '02', 'e383a440-6ad2-48bd-97cc-728986316c40'),
(98804934, 17, '02', '6aa913d2-6a1e-4583-8310-5f0efe3d1813'),
(98809299, 21, '02', 'e9347ea7-7881-473f-af51-9bc359359155'),
(98812357, 17, '02', '6aa913d2-6a1e-4583-8310-5f0efe3d1813'),
(98817755, 25, '02', 'e9347ea7-7881-473f-af51-9bc359359155'),
(98880029, 23, '02', '42e896de-b34f-4e4f-810d-ae57cb98fce7'),
(98882251, 20, '02', '9fbd119a-4f2f-400c-9e92-3513cb6b50ba'),
(98887657, 22, '02', '42e896de-b34f-4e4f-810d-ae57cb98fce7'),
(98890918, 15, '02', '43692490-d78b-43e2-ab08-9e31e265ab8d'),
(98891777, 13, '02', '43692490-d78b-43e2-ab08-9e31e265ab8d'),
(99027612, 23, '02', 'd1c1ddbb-a0f6-4594-9fe1-076576ea22df'),
(99047771, 22, '02', 'd1c1ddbb-a0f6-4594-9fe1-076576ea22df'),
(99920702, 23, '02', '9019038f-1cbd-4c42-b591-44f90ea86e36'),
(99951919, 22, '02', '9019038f-1cbd-4c42-b591-44f90ea86e36'),
(965550809, 55, '02', '3e2a9bbf-ed5e-4366-8057-b29310080986'),
(968809447, 20, '02', 'fa658202-a030-4d58-b50b-38c37ca19e47'),
(972210464, 25, '02', '3e2a9bbf-ed5e-4366-8057-b29310080986'),
(977910123, 55, '02', '7700b730-27dc-41be-8c73-835da9773d25'),
(978091729, 55, '02', '0f2c5c18-9586-4af5-aa7a-f60d5c47021a'),
(978340133, 13, '02', '755bba72-583a-44ca-81ca-1a5ed6d5fff9'),
(978349405, 15, '02', '755bba72-583a-44ca-81ca-1a5ed6d5fff9'),
(978542274, 25, '02', '0f2c5c18-9586-4af5-aa7a-f60d5c47021a'),
(979990271, 15, '02', '092fa833-55c7-4d22-bb09-947fc09c3ede'),
(982209139, 15, '02', 'eef76b3d-91ae-459d-8bad-1f508d8f42c7'),
(982341212, 12, '02', '9d661be3-8aa7-4fc8-adb7-f71dc5afc74b'),
(984447652, 12, '02', '9d661be3-8aa7-4fc8-adb7-f71dc5afc74b'),
(987509712, 13, '02', 'eef76b3d-91ae-459d-8bad-1f508d8f42c7'),
(987549355, 13, '02', '642fb96f-de85-4cbc-8a1c-12754f636dc1'),
(987550929, 15, '02', '642fb96f-de85-4cbc-8a1c-12754f636dc1'),
(988109445, 25, '02', '7700b730-27dc-41be-8c73-835da9773d25'),
(988581199, 13, '02', '2bdf40f7-c863-4bb8-980a-56ba81ff26de'),
(988701091, 15, '02', '2bdf40f7-c863-4bb8-980a-56ba81ff26de'),
(989970267, 13, '02', '092fa833-55c7-4d22-bb09-947fc09c3ede'),
(991650809, 15, '02', '448ad41b-7066-42ff-b9c8-c602028e4798'),
(991728383, 13, '02', '448ad41b-7066-42ff-b9c8-c602028e4798'),
(997710909, 13, '02', 'cf9a98a9-fa92-4a88-9d50-258286513e95'),
(997710929, 15, '02', '8ff1cf0a-55fb-4e88-b233-46121873beab'),
(997754527, 13, '02', '8ff1cf0a-55fb-4e88-b233-46121873beab'),
(997775212, 15, '02', 'cf9a98a9-fa92-4a88-9d50-258286513e95'),
(998540902, 15, '02', '5aae6b39-9513-4aa2-97b6-e7f8a784b20e'),
(998540929, 13, '02', '5aae6b39-9513-4aa2-97b6-e7f8a784b20e');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role`
--

CREATE TABLE `role` (
  `id_role` int(11) NOT NULL,
  `role_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `role`
--

INSERT INTO `role` (`id_role`, `role_name`) VALUES
(1, 'ROLE_ADMIN'),
(2, 'ROLE_USER');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id_user` varchar(500) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `login` varchar(100) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_at` varchar(200) DEFAULT NULL,
  `updated_at` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id_user`, `name`, `login`, `password`, `is_active`, `created_at`, `updated_at`) VALUES
('092fa833-55c7-4d22-bb09-947fc09c3ede', 'Juan Manuel Ramirez', 'juanmaramirez.futbol@gmail.com', '$2a$10$hOY5Ip4S8LCb.Wy.7u/VG.1UIsH8Tj7gAidkNhsiq7lSH2pA0Uwiq', 0, 'feb. 15, 2024 17:25:04 p. m.', 'feb. 15, 2024 17:25:04 p. m.'),
('0f2c5c18-9586-4af5-aa7a-f60d5c47021a', 'Gisella Gallardo', 'gisellagallardodepinilla@gmail.com', '$2a$10$qlszNc2CqGLN0dsAX3LJAel3T.Nz1u/yWtLzKZr1pxbFxOs9IQONO', 0, 'feb. 19, 2024 12:24:29 p. m.', 'feb. 19, 2024 12:24:29 p. m.'),
('1935c9f3-30d4-4237-93e1-782c0fc0264e', 'Martin Rivas', 'martinrivas.25@gmail.com', '$2a$10$Tygx5QOPZoLLYfifiZlqMemoD6rBPcIlmRCzU2U5ooBChn9rQyPJy', 1, 'feb. 13, 2024 13:08:08 p. m.', 'feb. 13, 2024 13:08:08 p. m.'),
('2bdf40f7-c863-4bb8-980a-56ba81ff26de', 'Rodrigo Quintana', 'doctorquintana@gmail.com', '$2a$10$OAZmW4SP35zRjbkD/V01Eev9dQHf/EX2RT.jc.0hgyCLFbFqpFN2G', 0, 'feb. 16, 2024 22:54:44 p. m.', 'feb. 16, 2024 22:54:44 p. m.'),
('36c42aa8-3ecc-4b0d-9b27-a12a45e5a249', 'Lionel Messi', 'lionel.messi.10@gmail.com', '$2a$10$xQBcngVrCar/hbHb4oS8m.eHsJ26JjvLoaSLoZTjDlOZVe5w1Q3mO', 1, 'feb. 08, 2024 15:43:28 p. m.', 'feb. 08, 2024 15:43:28 p. m.'),
('3e2a9bbf-ed5e-4366-8057-b29310080986', 'Jorge Valdivia', 'maginhovaldivia.10@gmail.com', '$2a$10$Fuick7s4PAbAWs5lJeI4UexiqsOnXp78fwS1wVXfelpxg.BgP9Dsu', 0, 'feb. 18, 2024 23:58:54 p. m.', 'feb. 18, 2024 23:58:54 p. m.'),
('42e896de-b34f-4e4f-810d-ae57cb98fce7', 'Marcelo Mendoza', 'marcelo.mendoza@gmail.com', '$2a$10$TB9o8oMwQmT86pBFrXUgBuKfiahXXmkKo5tN4zSfp/0jdi4eN4bsK', 1, 'feb. 12, 2024 13:27:51 p. m.', 'feb. 12, 2024 13:27:51 p. m.'),
('43692490-d78b-43e2-ab08-9e31e265ab8d', 'Pedro Lemebel', 'pedrolemebel.escritura@gmail.com', '$2a$10$GE2PpcF1kTdqZcKU1YPLfuoEJ6sn5UAPSPlOcQbBrOJ9QS3auuWEC', 0, 'feb. 15, 2024 16:58:00 p. m.', 'feb. 15, 2024 16:58:00 p. m.'),
('448ad41b-7066-42ff-b9c8-c602028e4798', 'Milovan Mirosevic', 'milomirosevic@gmail.com', '$2a$10$Ni0AnAuRgFO1N/n68GkMs.3FBGHVdbGyxxEC5mtSedDrPDNmtVHt6', 1, 'feb. 15, 2024 13:26:10 p. m.', 'feb. 15, 2024 13:26:10 p. m.'),
('5aae6b39-9513-4aa2-97b6-e7f8a784b20e', 'Juan Neme', 'juanneme.666@gmail.com', '$2a$10$HbQXLjDqGvyk6QgJ9u/3lekFYNzY5Nc8E7IfKzw5Kz.bSke1U0MxK', 1, 'feb. 15, 2024 16:10:58 p. m.', 'feb. 15, 2024 16:10:58 p. m.'),
('642fb96f-de85-4cbc-8a1c-12754f636dc1', 'Gabriel Marian', 'gabrielmarian.conciertos@gmail.com', '$2a$10$IoUAugD1wRxBGOJLVZy97OTsA8PJaeQqXKGwxiK3LIVYWz0zyAO5W', 1, 'feb. 15, 2024 13:21:56 p. m.', 'feb. 15, 2024 13:21:56 p. m.'),
('6711d5bd-3629-4aea-95a7-6ea71fccfce7', 'Angel Di Maria', 'angelitodimaria.11@gmail.com', '$2a$10$Ac.z0Xp.XM/eFa0UVMZkoeVrnp7eOF8Fea3WRuxEWBc5BTw1M2b9O', 1, 'feb. 07, 2024 17:56:02 p. m.', 'feb. 07, 2024 17:56:02 p. m.'),
('6aa913d2-6a1e-4583-8310-5f0efe3d1813', 'Oscar Ruggeri', 'cabezon.ruggeri.futbol@gmail.com', '$2a$10$/LcOiqsy7ON.vy.DuIBe1OUAXzY7KCXx4EMD4W12GpaLv/7UJatC2', 1, 'feb. 12, 2024 16:42:46 p. m.', 'feb. 12, 2024 16:42:46 p. m.'),
('755bba72-583a-44ca-81ca-1a5ed6d5fff9', 'Bernardo Ortuzar', 'bernardo.ortuzar.abogado@gmail.com', '$2a$10$DfQVvVZJazW9AbhP2.QIKu7DjYGDWol7uA5gvkfAM6GeT69SGMlF6', 0, 'feb. 16, 2024 13:25:05 p. m.', 'feb. 16, 2024 13:25:05 p. m.'),
('7700b730-27dc-41be-8c73-835da9773d25', 'Gustavo Huerta', 'gustavohuerta.periodismo@gmail.com', '$2a$10$Q.fj8B8sNw6LnwzdUxwZ1O7vDGxQb8LkEM9uuRzGT4ScvFLq3HHg2', 0, 'feb. 18, 2024 21:09:59 p. m.', 'feb. 18, 2024 21:09:59 p. m.'),
('80579c9c-d479-46a6-8bbc-29acb2d7a8cc', 'Alexis Sanchez', 'alexis.sanchez.as07@gmail.com', '$2a$10$lHQB6ar46cqu69N2ogZVKuVWIRHKNHW8PrvsbSz7zXbO7lLf1yA/W', 1, 'feb. 08, 2024 15:51:05 p. m.', 'feb. 08, 2024 15:51:05 p. m.'),
('8ff1cf0a-55fb-4e88-b233-46121873beab', 'Johann Schmidt', 'redskull.666@gmail.com', '$2a$10$Oua55MKAh9Zx2nw8CFq0X.Uwrq2X8Wk.vfovp99X.R61I2dfXs3gy', 0, 'feb. 15, 2024 16:47:04 p. m.', 'feb. 15, 2024 16:47:04 p. m.'),
('9019038f-1cbd-4c42-b591-44f90ea86e36', 'Rogelio Delgado', 'rogeliodelgado@gmail.com', '$2a$10$g280LgxSDnEbBbJVYbfw.O7umUxG7g4okCTf1vsMx/ih00GStGgzG', 1, 'feb. 12, 2024 13:51:05 p. m.', 'feb. 12, 2024 13:51:05 p. m.'),
('9d661be3-8aa7-4fc8-adb7-f71dc5afc74b', 'Ivan Zamorano', 'bambam.zamorano@gmail.com', '$2a$10$ZjvzUmaIznShotJSXdF88e6w5TrVpDUrjbVnpBbBacmcdpRoNKIP.', 1, 'feb. 07, 2024 17:45:20 p. m.', 'feb. 07, 2024 17:45:20 p. m.'),
('9fbd119a-4f2f-400c-9e92-3513cb6b50ba', 'Nicolas Massú', 'nico.massunico@gmail.com', '$2a$10$0Tpbf.5NoioeJ0xM4F9jPOaVVqFBJUZd9NpL6RMbC6THbjfrnI6M2', 1, 'feb. 08, 2024 12:21:02 p. m.', 'feb. 08, 2024 12:21:02 p. m.'),
('c1273773-194d-4ed3-ad38-fc87ac72a574', 'Gabriel Mendoza', 'cocamendoza@gmail.com', '$2a$10$N.klSrkvQ/q2yJIPEvULoeVbI8PWqYyND.mtbAxBV7pFBR7mzqa2O', 1, 'feb. 08, 2024 12:25:54 p. m.', 'feb. 08, 2024 12:25:54 p. m.'),
('c1980397-4b1d-439a-b81b-e6f36bb0bd3a', 'Rogelio Morante', 'rolegiomorante.abogado@gmail.com', '$2a$10$RaKkaCm1yw2FzMicbWTX.e9W6N3Av6Pil3JxxIQiDgsW7hvE7HwfS', 1, 'feb. 12, 2024 13:22:38 p. m.', 'feb. 12, 2024 13:22:38 p. m.'),
('cf9a98a9-fa92-4a88-9d50-258286513e95', 'Patricio Laguna', 'patolaguna.eventos@gmail.com', '$2a$10$en1xR0JzK8GB1G1XCcMetet2bLxFeHsilRWPeTLQHi0BByaUVTole', 1, 'feb. 15, 2024 13:46:18 p. m.', 'feb. 15, 2024 13:46:18 p. m.'),
('d1c1ddbb-a0f6-4594-9fe1-076576ea22df', 'Diego Maradona', 'pelusa.10@gmail.com', '$2a$10$I6mNfVf.c8SM1ki9hdoSUeviXUeZIHASImHogIcAgwpRDHCoBNUbC', 1, 'feb. 12, 2024 16:29:04 p. m.', 'feb. 12, 2024 16:29:04 p. m.'),
('e383a440-6ad2-48bd-97cc-728986316c40', 'Crespita Rodriguez', 'crespitarodriguez.65@gmail.com', '$2a$10$VAr1R7RyY21KVmeWa.oBpOqbR/qC1X.HWnq3KSkoHOdsfsFzIdORq', 1, 'feb. 12, 2024 17:24:48 p. m.', 'feb. 12, 2024 17:24:48 p. m.'),
('e9347ea7-7881-473f-af51-9bc359359155', 'Cristiano Ronaldo', 'cristianoronaldo.cr7@gmail.com', '$2a$10$4.eQ/utstIyqnfHsE2HaouWgnV4f4.zhOYlfVrUmD4z9i6elrBcrO', 1, 'feb. 08, 2024 15:55:38 p. m.', 'feb. 08, 2024 15:55:38 p. m.'),
('ece1ab31-dda6-4927-b113-1294b57bc4e5', 'David Bisconti', 'davidnazarenobisconti.goleador@gmail.com', '$2a$10$q3ErHmL.vyH5EgnBOmGeyu3A3EHLCNmOaSMYXd6vtKAjOym1WKzcW', 1, 'feb. 12, 2024 10:57:21 a. m.', 'feb. 12, 2024 10:57:21 a. m.'),
('eef76b3d-91ae-459d-8bad-1f508d8f42c7', 'Clemente Valencia', 'clementevalencia@gmail.com', '$2a$10$Iuq4Sq.54wlrsToR90l2Qu1DJnWX.4HcJdziKpBQHoyDaQMr0Z34.', 1, 'feb. 14, 2024 12:43:56 p. m.', 'feb. 14, 2024 12:43:56 p. m.'),
('fa658202-a030-4d58-b50b-38c37ca19e47', 'Marcelo Vega', 'tobivega@gmail.com', '$2a$10$iv1cl6Glx3kMuLQrljljMeoGXLnUJov6Wnd9qTi9WTOitww9kXq8e', 1, 'feb. 08, 2024 15:33:42 p. m.', 'feb. 08, 2024 15:33:42 p. m.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_role`
--

CREATE TABLE `user_role` (
  `user_id` varchar(500) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `user_role`
--

INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
('092fa833-55c7-4d22-bb09-947fc09c3ede', 2),
('0f2c5c18-9586-4af5-aa7a-f60d5c47021a', 2),
('1935c9f3-30d4-4237-93e1-782c0fc0264e', 2),
('2bdf40f7-c863-4bb8-980a-56ba81ff26de', 2),
('36c42aa8-3ecc-4b0d-9b27-a12a45e5a249', 2),
('3e2a9bbf-ed5e-4366-8057-b29310080986', 2),
('42e896de-b34f-4e4f-810d-ae57cb98fce7', 2),
('43692490-d78b-43e2-ab08-9e31e265ab8d', 2),
('448ad41b-7066-42ff-b9c8-c602028e4798', 2),
('5aae6b39-9513-4aa2-97b6-e7f8a784b20e', 2),
('642fb96f-de85-4cbc-8a1c-12754f636dc1', 2),
('6711d5bd-3629-4aea-95a7-6ea71fccfce7', 2),
('6aa913d2-6a1e-4583-8310-5f0efe3d1813', 2),
('755bba72-583a-44ca-81ca-1a5ed6d5fff9', 2),
('7700b730-27dc-41be-8c73-835da9773d25', 2),
('80579c9c-d479-46a6-8bbc-29acb2d7a8cc', 2),
('8ff1cf0a-55fb-4e88-b233-46121873beab', 2),
('9019038f-1cbd-4c42-b591-44f90ea86e36', 2),
('9d661be3-8aa7-4fc8-adb7-f71dc5afc74b', 2),
('9fbd119a-4f2f-400c-9e92-3513cb6b50ba', 2),
('c1273773-194d-4ed3-ad38-fc87ac72a574', 2),
('c1980397-4b1d-439a-b81b-e6f36bb0bd3a', 2),
('cf9a98a9-fa92-4a88-9d50-258286513e95', 2),
('d1c1ddbb-a0f6-4594-9fe1-076576ea22df', 2),
('e383a440-6ad2-48bd-97cc-728986316c40', 2),
('e9347ea7-7881-473f-af51-9bc359359155', 2),
('ece1ab31-dda6-4927-b113-1294b57bc4e5', 2),
('eef76b3d-91ae-459d-8bad-1f508d8f42c7', 2),
('fa658202-a030-4d58-b50b-38c37ca19e47', 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `phone`
--
ALTER TABLE `phone`
  ADD PRIMARY KEY (`number`),
  ADD KEY `fk_phone_user1` (`phone_user`);

--
-- Indices de la tabla `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indices de la tabla `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `fk_user_has_role_user` (`user_id`),
  ADD KEY `fk_user_has_role_role` (`role_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `role`
--
ALTER TABLE `role`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `phone`
--
ALTER TABLE `phone`
  ADD CONSTRAINT `fk_phone_user1` FOREIGN KEY (`phone_user`) REFERENCES `user` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `fk_user_has_role_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id_role`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_has_role_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
